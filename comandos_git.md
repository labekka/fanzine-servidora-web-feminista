# Comandos básicos de git:

Clonar el repo en su compu:

`git clone https://0xacab.org/labekka/fanzine-servidora-web-feminista.git`

Situarse en el repo:

`cd /su_ruta/fanzine-servidora-web-feminista`

Inicializar el repo:

`git init`

Bajarse los cambios que hayan hecho las compañeras:

`git pull`

Moverse de rama:

`git checkout nombre_rama`

Añadir sus cambios:

```
git add .
git commit -am "comentario"
git push origin nombre_rama`
```
(te pide la contraseña)

¡El proceso es más importante que el producto! ¡Disfrútenlo! ¡Disfruten de los errores también!
