# Fanzine - Cómo montar una servidora feminista con una conexión casera

Hola amiga, bienvenida al repositorio del fanzine 'Cómo montar una servidora feminista con una conexión casera'. Puedes leer la versión en línea o descargar los PDF para imprimir en [https://labekka.red/servidoras-feministas/](https://labekka.red/servidoras-feministas/). 

Este será un espacio para:
* Mantener actualizado el fanzine.
* Corregir errores
* Agregar pasos, opciones, o maneras de hacer las cosas. 
* Profundizar las explicaciones. 
* Traducir el fanzine a otros idiomas. 

Todos los cambios se harán en el máster excepto por las traducciónes. Cada uno de los idiomas de la traducción (inglés, portugués y francés, por ahora) estará en un branch específico. 

Cualquier duda pueden escribirnos a hacklabfeminista[a]riseup.net

¡Esperamos sus pull requests! 

Abrazos,

la_bekka
