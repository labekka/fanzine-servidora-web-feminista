---
layout: post
title:  "Parte I - Una infraestructura feminista"
date:   2019-10-9 20:05:00 +0200
last_modified_at:   2023-08-01 00:53:00 +0000
categories: servidoras-feministas
tags: fanzine
---
<div class="menu-zine inner">
  <ul>
  <li><a href="/servidoras-feministas/"><i class="fa fa-eye"></i>Volver al índice</a></li>
    <li><a href="/media/Parte-1_con_portada.pdf" download=""><i class="fa fa-print"></i>Imprimir fanzine</a></li>
    <li><a href="/media/Parte1-web.pdf"><i class="far fa-file-alt"></i>PDF online</a></li>
    <li><a href="/servidoras-feministas/2019/10/09/fanzine-parte-2.html"><i class="fa fa-eye"></i>Ir a Parte II</a></li>
  </ul>
</div>

{: #contenidos }
## Contenidos

1. [¡Amigas!](#amigas)
2. [Instrucciones para usar esta guía](#instrucciones)
3. [¿Qué necesitan?](#necesitan)
4. [Glosario](#glosario)

![Infraestructura feminista](/media/infrafem2.png)

{: #amigas }
## 1. ¡Amigas!

Qué bueno vernos por aquí. No sabemos cómo llegó esta guía a sus manos: si se las recomendó alguien, si la encontraron de casualidad en una feria o si apareció después de estar navegando de enlace en enlace buscando alguna instrucción más o menos clara sobre cómo tener una web propia. Sea como sea que hayan llegado hasta aquí la buena noticia es que nos encontramos.

Esta guía es el resultado de más de un año de trabajo en [la_bekka](https://www.labekka.org), el espacio hackfeminista de la [Eskalera Karakola](https://eskalerakarakola.org/) (EKKA), una casa pública transfeminista -antigua casa okupa- de la ciudad de Madrid. Desde finales del año 2017 nos juntamos semanalmente para aprender, compartir y montar nuestra infraestructura digital. Unos meses más tarde, en marzo de 2018, nos reunimos un grupo de servidoras feministas en [Calafou](https://calafou.org/) para conocernos y pensar juntas qué significaba construir infraestructura feminista. Pensamos en las máquinas (digitales y analógicas) y en código y conexiones. Pero también pensamos en nosotras, en nuestros cuerpos, nuestras relaciones, nuestros tiempos. Tratamos de identificar qué es aquello que sostiene nuestras prácticas: el aire, el cariño, la electricidad, la empatía, el descanso. Al día de hoy todavía seguimos pensando.

De regreso, en Madrid, vimos que en la EKKA había equipos viejos de proyectos de otros años así que hicimos un poco de arqueología informática. Revisamos todos los equipos que encontramos, reciclamos los que pudimos y nos deshicimos de los demás (llevándolos al punto limpio). Exploramos viejos discos duros y encontramos memorias de quienes habían transitado el espacio antes que nosotras. Respaldamos todo porque lo que se borra se olvida y la memoria alimenta el corazón de nuestro movimiento.

Así que una vez que nos enfrentamos a los equipos que teníamos comenzamos a soñar a lo grande. Queríamos tener de todo: desde un repositorio de publicaciones y audiovisuales transfeministas, hasta instalar servicios web que las organizaciones sociales pudieran aprovechar: pad, calendario, web, listas de correo, archivos compartidos, etc. Peleamos con las bases de datos, con el node.js, con el PHP, con los routers. ¡Grrrr! Finalmente nos decidimos por empezar a armar una servidorcita que alojara nuestro sitio web.

Antes de continuar, queremos aclarar que un servidor no es más que una computadora, normalmente conectada a una red o a Internet, que ofrece (solemos decir "que sirve") servicios. Si es un servidor web pues lo que sirve es una web, si es un servidor de correo lo que ofrece es un servicio de correo, etc. Es una máquina que está ahí a la escucha esperando a que otras maquinas le pidan una tarea, una petición web, el envío de un correo, etc. Desde los espacios transhackfeministas que habitamos solemos decir "servidora" cuando es una maquina que sirve contenidos o servicios feministas, por eso a nuestra maquinita la llamamos servidora o servidorcita porque es es chiquita.

La idea de usar nuestros propios equipos, reusando viejas piezas y cables, aprovechando dispositivos baratos de los que pudimos echar mano, tiene un claro fundamento político: **creemos en la soberanía tecnológica, en nuestra capacidad de decidir qué Internet queremos habitar**. Y la Internet que queremos es una Internet transfeminista, ecológica, distribuida y descentralizada, al servicio de los movimientos sociales. Una Internet que escapara de la lógica impuesta desde *Sillicon Valley* que nos empuja al consumo acelerado, la datificación y mercantilización de cada aspecto de nuestra vida. Queremos que nuestra memoria descanse en nuestras máquinas, administradas por nosotras y bajo nuestros principios: el anonimato, el aprendizaje y conocimientos colectivos, la distribución del poder y la cultura libre. Queremos tener control sobre todas nuestras historias, nuestras recetas, nuestras alegrías y pesares, nuestros mitos y leyendas, nuestras canciones y poemas, nuestros imaginarios y miedos y fantasías. Porque **nuestros datos, digitales y analógicos, no pueden ser utilizados para alimentar al monstruo**.

Finalmente montamos una servidora web utilizando los recursos que teníamos a mano y que pensamos que eran fácilmente accesibles. Para el hardware elegimos una Raspberri Pi, una pequeña máquina de bajo costo al alcance de muchas y con recursos suficientes para alojar una web. Es verdad que no todas tenemos la posibilidad de hacernos con una. No importa, podemos seguir las instrucciones de esta guía usando una laptop vieja, una computadora de escritorio que armamos con partes a lo Frankenstein. Pero otra de las ventajas de usar una Raspberry Pi es su bajo consumo eléctrico. Aunque varíe según los servicios que esté corriendo, el consumo promedio para nuestra servidora será de unos 3 watts por hora, lo que nos da un consumo mensual de 2,15 kilowatts por mes si la tenemos prendida todo el día. Así que por más cara que esté la electricidad en donde vivan no creemos que supere el dólar mensual. También usaremos Raspberri Pi OS, una distribución de GNU/Linux desarrollada para Raspberri Pi; Apache, porque es un servidor web con mucha documentación y algunas ya lo conociamos un poco; y Jekyll, un generador de sitios estáticos que reduce la cantidad de recursos necesarios para funcionar.

Estas fueron nuestras elecciones a partir de lo que teníamos. Pero también pueden remplazar estas opciones por otras a partir de lo que ustedes tengan a mano o con lo que quieran experimentar. Pueden usar una máquina de escritorio o laptop vieja, pueden elegir usar Debian u también optar por nGinx para su servidora. Todo es posible y de eso se trata: que hagamos nuestras propias elecciones y aprendamos en el camino.

Estamos felices de habernos encontrado y acompañarlas en este camino. Esperamos que asuman esta tarea con el entusiasmo con el que lo hicimos nosotras y aprovechen esta oportunidad para aprender, construir autonomía y, sobre todo, divertirse entre amigas. Queremos agradecer a todas las personas que colaboraron en la construcción de esta guía. A la EKKA por ser nuestra casa, a Calafou y el encuentro de infraestructura feminista por inspirarnos, la lista femservers por ser nuestra comunidad de apoyo, y a Tes, Marta S., Spideralex y Dulzet, por su lectura atenta y aportes cariñosos. Fuimos muchas las que dedicamos nuestro tiempo a este proceso. A todas ellas/nosotras un reconocimiento enorme y agradecimiento por este pequeño aporte a la soberanía tecnológica del movimiento transhackfeminista.

¡Un abrazo!

*la_bekka, octubre 2019*

<div class="volver-tabla"><a href="#contenidos">Volver a la tabla de contenidos ⤴</a></div>

{: #instrucciones }
## 2. Instrucciones para usar esta guía

Escribimos estas líneas con la intención de hacer una guía pero que no fuera un manual al uso. Muchas veces las instrucciones que encontramos en foros o blogs dan muchos conocimientos por sentado que no necesariamente tenemos y además la gran mayoría está en inglés. Esto genera mucha frustración: pensamos que nada nos funciona, que no sabemos lo suficiente o que no servimos para esto. Tranquilas, ¡a nosotras también nos pasa! Por eso queremos explicar con detalle cada uno de los pasos de este proceso, como nos hubiera gustado que nos lo explicaran a nosotras y en castellano. Para los recursos que solo encontramos en inglés recomendamos el traductor deepl.com que es muy potente y además respeta la privacidad.

¿A quién le hablamos? Porque si bien es verdad que es el lenguaje de esta guía es simple y los pasos los damos de manera detallada, es cierto que necesitamos tener algunos conocimientos previos de informática. Como mínimo haber instalado un sistema operativo anteriormente. Esto no es para disuadir a quien no sabe nada de poder montar su servidora web. ¡Nada más lejos de eso! Sino para decirles que se queden tranquilas si las cosas no funcionan a la primera, que no se frustren. Simplemente tendrán que ir más despacio e ir aprendiendo paso a paso.

Proponemos, entonces, un recorrido cronológico del proceso. Pensamos en una guía modular, en distintos volúmenes, que ofreciera más agilidad a la hora de leer. Elegimos el formato fanzine, para su versión impresa, porque nos remitía a toda la cultura del DIY y queríamos traer el espíritu de la autoedición al mundo tecnológico. También tenemos una versión digital para poder alimentar este texto que esperemos esté en beta permanente. Por eso proponemos cinco partes: I. "Una infraestructura feminista", a modo de introducción; II. ¡Hola maquinita!, para instalar nuestro sistema operativo; III. "De máquina a servidora", para instalar Apache; IV. "Securicemos nuestra servidora", con medidas y consejos de seguridad; y, V. "Nuestra web estática", para aprender a usar Jekyll.

Queríamos recordarles que nosotras no somos profesionales de la administración de sistemas. Es verdad que algunas tenemos formación en informática pero otras no, hemos aprendido durante estos meses. Pusimos mucho esfuerzo en dar indicaciones que no las lleven a situaciones de vulnerabilidad. Abrir los puertos de nuestros *routers* puede exponernos a ataques si no tomamos los recaudos necesarios. Por eso tomamos varias precauciones: investigamos, preguntamos y probamos todas las medidas de seguridad que pudimos para que nuestra servidora no nos exponga. Pero como estas cosas cambian todo el tiempo las animamos a visitar nuestra página web [https://labekka.red](https://labekka.red) para leer cualquier actualización de herramientas o medidas de seguridad que hagamos.

Las instrucciones de esta guía las pensamos para ser ejecutadas desde GNU/Linux, en concreto Debian. ¿Por qué? Porque somos usuarias de software libre y Debian es un sistema operativo libre que respeta las libertades de poder usar, distribuir, ver cómo está hecho, modificar y distribuir esas modificaciones. Muchas de estas instrucciones también sirven para MacOS ya que también es un sistema basado en UNIX. Si usan Windows ofrecemos algunos enlaces con instrucciones específicas. Eso sí, usaremos mucho la consola, así que tendremos una excusa para familiarizarnos con el uso de esta potente herramienta.

Para editar archivos de configuración usaremos un editor de textos muy sencillo de usar que se llama "nano". Tenemos que aprender tres atajos de teclado sencillitos para que ustedes se sientan cómodas usándolo y nosotras no parezcamos loros repitiendo siempre lo mismo: para guardar apretamos `Ctrl + O` (la letra o). Eso nos mostrará las líneas que vamos a escribir y en qué ubicación. Para confirmár aprentamos *enter*. Y, luego, para salir escribimos `Ctrl + X`.

Otra de las bellezas de esta guía es que está redactada en muchos castellanos distintos como reflejo de nuestra propia diversidad. Algunas de nosotras a las cosas que nos gustan las llamamos guays, copadas o chachi. Hicimos un esfuerzo por unificarlo, no como negación de nuestra diversidad sino con el objetivo de facilitar la lectura. Seguramente quedarán expresiones que quizás les suenen raras, ¡así es la mezcla!

Queremos recomendarles que documenten su proceso. Esta guía es el resultado del nuestro, pero no hay dos procesos iguales. Cada uno tiene sus contextos, las particularidades de quienes participamos en ellos. Poder alimentar un registro de las decisiones que tomamos, de los pasos que dimos, de las dificultades que afrontamos nos permitirá reconstruir el proceso para ajustarlo, revisarlo o multiplicarlo. Lo pueden hacer digital o analógicamente con un cuadernito.

Por último, también elaboramos un glosario que en el que intentamos explicar con palabras sencillas algunos de los conceptos que utilizaremos en toda la guía. Las palabras que estén en el glosario están resaltadas a lo largo del texto para que sepan que pueden consultarlas. Ninguna nace sabiendo, ninguna tiene por qué conocerlo todo. Para eso estamos aquí: para ser parte de esta construcción colectiva.

Cualquier duda, aporte o pregunta pueden hacerla a [hacklabfeminista@riseup.net](mailto:hacklabfeminista@riseup.net). Pueden descargar nuestra llave GPG de [https://labekka.red/la_bekka-public.asc](https://labekka.red/la_bekka-public.asc). El *fingerprint* es 242D 8159 9124 E231 EC51 B813 0E4C A504 9F28 BAEB.

<br>

![Escribinos](/media/escribinos.png)

<div class="volver-tabla"><a href="#contenidos">Volver a la tabla de contenidos ⤴</a></div>

{: #necesitan}
## 3. ¿Qué necesitan?

* Una Raspberry Pi 3 modelo B+ (Suponemos que se puede hacer con otros modelos. Tuvimos problemas con una Pi 1 Model B+ a la hora de configurar Certbot)
* Una tarjeta micoSD para la Raspberry Pi. Mínimo 8GB, recomendado 32GB. Por lo general estas microSD vienen con un adaptador SD para poder usarlas en nuestras computadoras.
* Transformador para enchufar la Raspberry Pi a la corriente eléctrica.
* Conexión a Internet.
* Conexión a red eléctrica.
* Acceso de administradora al *router* por el que te llega la conexión a Internet.
* Cable de red.
* Computadora/ordenador (sólo para la configuración y cada vez que quieras actualizar contenido de tu web).
* Lector de tarjetas microSD (puede ser que la computadora ya lo tenga integrado).
* Monitor con salida HDMI (sólo para la instalación del sistema operativo).
* Cable HDMI (sólo para la instalación del sistema operativo).
* Teclado (sólo para la instalación del sistema operativo).
* Ratón/mouse (sólo para la instalación del sistema operativo).
* Un dominio o algo de dinero para comprar uno (opcional, también podemos montar la servidora sin dominio propio).
* Tiempo.
* Paciencia.
* Algunos conocimientos de GNU/Linux y de redes (¡comandos básicos y sin miedo!).
* Inquietud e ilusión por la soberanía tecnológica.
* Amigas, porque si el proceso es compartido es mejor.

<div class="volver-tabla"><a href="#contenidos">Volver a la tabla de contenidos ⤴</a></div>

{: #glosario }
## 4. Glosario

No se asusten con la terminología. Iremos poco a poco y, en caso de dudas, siempre están Internet, las amigas y Wikipedia para ayudarnos. Aquí explicamos algunos conceptos que nos van a ayudar:

Archivo o fichero
: Es un conjunto de bits identificados por un nombre y la ubicación de donde están alojados. Es una analogía de los documentos analógicos. En Linux se suele decir "fichero" y en Windows se suele decir "archivo", en ambos casos es una traducción de "file".

Certificados SSL/TLS
: Es una firma digital que permite cifrar la información que se envía entre el servidor y el navegador que está accediendo a nuestra web, gracias al sistema de cifrado basado en protocolos SSL/TLS. Si nuestro sitio web cuenta con un certificado nos aseguramos -por un lado- que la comunicación no pueda interceptarse y leerse y -por otro- damos garantías sobre la identidad del sitio web con el que se produce la comunicación.

Consola de comandos
: Es un método para dar instrucciones a un sistema operativo a través de líneas de texto simple. Cotidianamente se la llama terminal, línea de comandos o Shell, aunque no sean exactamente lo mismo.

Daemon o demonio
: Es un tipo de proceso informático que se ejecuta en segundo plano sin intervención de la usuaria.

Dirección IP
: Es una dirección que identifica a cualquier dispositivo que esté utilizando el protocolo de Internet. Tiene la forma de 4 números separados por puntos, por ejemplo: 192.168.1.1

Dirección IP privada/local
: Es la dirección IP que suele empezar por los números 192.168.XXX.XXX y que identifica a un dispositivo en una red interna (también se dice privada o local), fuera de Internet. Por ejemplo, en una casa, en una oficina o una comunidad.

Dirección IP pública
: Es una dirección IP con la que se identifica a un dispositivo que "sale" y es accesible desde Internet.

Dirección MAC
: Dirección de 6 pares de caracteres separados por ":" y que identifica a una tarjeta/placa/interfaz de red de forma única. Esta dirección la asigna el fabricante de la tarjeta y es única en el mundo. Por ejemplo, `00:0a:95:9d:68:16`.

Directorio
: Es lo que en Windows llamamos "carpetas", un contendor virtual que agrupa una serie de archivos. Es la traducción de "directory".

Directorio root de Apache
: se suele llamar así al directorio */var/www/html* donde se copian los archivos HTML, CSS y el contenido multimedia que va a servir el servidor web. Allí también se guarda el archivo .htaccess.

Dominio de Internet
: Es un nombre único que identifica a un sitio web en Internet. Traduce una dirección IP en palabras más fáciles de identificar y recordar. Por ejemplo, el dominio de 91.198.174.192 es wikipedia.org.

DNS (_Domain Name Server_)
: Es el sistema que permite asociar un nombre de dominio a una dirección IP.

FTP (_File Transfer Protocol_)
: Es el protocolo de transferencia de archivos.

*Firewall* (cortafuegos)
: Es un sistema (hardware, software o ambos) que bloquea el acceso no autorizado a una red.

Firmware
: Es el software base (como el sistema operativo pero muy ligero) que viene instalado en dispositivos como routers, cámaras, impresoras, etc.

Host
: Dispositivo conectado en la red.

HTTP (_Hiper Text Transfer Protocol_)
: Protocolo que permite la transferencia de información en la web.

HTTPS
: Básicamente es la versión segura del HTTP que utiliza una capa de cifrado SSL/TLS.

Interfaz de red
: Es la placa de red, el hardware que permite conectar un dispositivo a la red.

Interfaz gráfica de usuario (GUI, por sus siglas en inglés)
: Es un programa que utiliza imágenes y elementos gráficos para interpretar la información y las posibles acciones que la usuaria puede ejecutar sobre ella.

ISP (Proveedor de Servicios de Internet)
: La compañia que contratas para tener Internet en tu casa.

LAN (_Local Area Network_)
: Es una red local que conecta dispositivos en un área limitada. En una casa, los dispositivos conectados a un mismo *router* forman una LAN o red interna.

Logs
: Son los registros o historial de todas las acciones o eventos que afectan a un proceso en un sistema informático. Ante una falla son útiles como evidencia e información detallada de por qué sucedió lo que sucedió.

Paquete
: es un conjunto de datos que puede referirse a un paquete de datos en el tráfico de una conexión o a paquete de software.

Protocolo
: serie de instrucciones que permiten a dos o más sistemas comunicarse.

Puerto de Internet
: Es un valor que se usa para cada una de las aplicaciones que se conectan a un mismo host. Hay muchos puertos por defecto: 80 para la web, 21 para el FTP, etc.

Servicio (también llamado demonio, daemon o programa residente)
: Es un tipo especial de proceso informático que se ejecuta en segundo plano en vez de ser controlado directamente por una usuaria.

Servidor/a
: Es una computadora (hardware y software) que "ofrece" un servicio: web, *mails*, *streaming*, etc. En esta guía llamamos "servidora" a la Rasberry Pi que servirá nuestra web.

Servidor web
: Es un programa informático que permite que una página web alojada en el servidor sea accesible desde Internet a través de peticiones HTTP que las usuarias hacen desde sus navegadores: escribir una URL, hacer clic en un *link*, ver una foto, etc.

Raspberry Pi
: Es una pequeña computadora de placa reducida y de bajo coste desarrollado en el Reino Unido por la Fundación Raspberry Pi, con el objetivo de estimular la enseñanza de informática en las escuelas. Su precio aproximado es de 30 dolares. Al no estar publicada su licencia no dejan del todo claro si es hardware libre, pero permiten su uso libre tanto a nivel educativo como personal.

Repositorios
: Son como bibliotecas, un espacio donde se almacenan, se organizan y se mantienen una serie de recursos; en este caso, programas informáticos, librerías, etc.

*Root* (raíz)
: En los sistemas operativos basados en UNIX es una superusuaria que tiene todos los privilegios posibles.

Shell
: Ver "Consola de comandos".

Sudo
: Es un programa de los sistemas operativos basados en UNIX que otorga a las usuarias la posibilidad de acceder temporalmente y de manera segura a los privilegios de una superusuaria (*root*).

TCP (_Transmission Control Protocol_)
: Es el protocolo básico de Internet que permite las conexiones entre redes de ordenadores y se asegura de que los paquetes de datos lleguen a destino.

TTL (_Time to Live_)
: Expresado en segundos, indica la vida útil de los datos en un ordenador o una red antes de ser descartado o devuelto a su origen.

UNIX
: Es un sistema operativo que de alguna manera fue la "madre" de otros sistemas operativos como GNU/LInux, MacOs, FreeBSD y otros.

WLAN (_Wireless Local Area Network_)
: Red interna inalámbrica, su *W* inicial la toma de Wireless.

WAN (_Wide Area Network_)
: No es exactamente lo mismo que Internet pero para este tutorial lo podemos considerar como tal.

Web
: ¡Ojo! la web no es sinónimo de Internet, sino tan solo uno de sus servicios basados en el protocolo HTTP.

¿Hay algún concepto que no esté listado en este glosario? Revisen nuestra página web [https://www.labekka.red](https://labekka.red) o busquen en Wikipedia.

<div class="volver-tabla"><a href="#contenidos">Volver a la tabla de contenidos ⤴</a></div>
